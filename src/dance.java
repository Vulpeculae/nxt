import lejos.nxt.Button;
import lejos.nxt.Motor;

public class dance {
	  public static void main(String[] args) {
		  Button.ENTER.waitForPressAndRelease();
			Motor.A.stop(true);
			Motor.C.stop();
			Motor.A.backward();
			Motor.C.backward();
			try {
				Thread.sleep(13500);
			} catch(InterruptedException e) {}
			Motor.A.stop(true);
			Motor.C.stop();
			Motor.C.forward();
			Motor.A.forward();

			try {
				Thread.sleep(13500);
			} catch(InterruptedException e) {}
			Motor.A.setSpeed(200);
			Motor.C.setSpeed(200);
			Motor.A.stop(true);
			Motor.C.stop();
			Motor.A.backward();
			Motor.C.forward();
			try {
				Thread.sleep(4000);
			} catch(InterruptedException e) {}
			Motor.A.setSpeed(300);
			Motor.C.setSpeed(300);
			Motor.A.stop(true);
			Motor.C.stop();
			Motor.C.backward();
			Motor.A.forward();
			try {
				Thread.sleep(4000);
			} catch(InterruptedException e) {}
			Motor.A.setSpeed(600);
			Motor.C.setSpeed(600);
			Motor.A.stop(true);
			Motor.C.stop();
			Motor.A.backward();
			Motor.C.forward();
			try {
				Thread.sleep(4000);
			} catch(InterruptedException e) {}
	  }
}
