
public interface FahrzeugInterface {
public float getDurchmesserReifen();
public float getAbstandAchse();
public int getGeschwindigkeitLinks();
public int getGeschwindigkeitRechts();	
public void setGeschwindigkeit(int links, int rechts);
public void fahreVorwaerts();
public void fahreVorwaertsStrecke(int streckeInCm);
public void fahreVorwaertsZeit(int zeitInSek);
public void fahreRueckwaerts();
public void fahreRueckwaertsStrecke(int streckeInCm);
public void fahreRueckwaertsZeit(int zeitInSek);
public void dreheRechts();
public void dreheRechtsGrad(int winkel);
public void dreheRechtsZeit(int zeitInSek);
public void dreheLinks();
public void dreheLinksGrad(int winkel);
public void dreheLinksZeit(int zeitInSek);
public void stoppe();
}
