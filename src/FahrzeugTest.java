import lejos.nxt.Button;

public class FahrzeugTest {

  // Anfang Attribute
  // Ende Attribute



  // Anfang Methoden
  public static void main(String[] args) {

  Button.ENTER.waitForPressAndRelease();
    FahrzeugInterface einFahrzeug = new Fahrzeug(360,360);    
    //Fahrzeugnamen aendern
    // Speed-Default-Wert ist 360?!

    // 1. Aufgabe
    // ????
    for (int i = 1; i <= 2; i++) {
      einFahrzeug.fahreVorwaertsStrecke(50);
      
      einFahrzeug.dreheRechtsGrad(90);
      einFahrzeug.fahreVorwaertsStrecke(20);
      einFahrzeug.dreheRechtsGrad(90);
    }

    // 2. Aufgabe
    // ????
    for (int i = 1; i <= 36; i++) {
      einFahrzeug.fahreVorwaertsStrecke(5);
      einFahrzeug.dreheLinksGrad(10);
    }

    // 3. Aufgabe
    einFahrzeug.setGeschwindigkeit(600,600);
    // ????
    for (int i = 1; i <= 2; i++) {
      einFahrzeug.fahreVorwaertsStrecke(50);
      einFahrzeug.dreheRechtsGrad(90);
      einFahrzeug.fahreVorwaertsStrecke(20);
      einFahrzeug.dreheRechtsGrad(90);
    } 
    
    // 4. Aufgabe
    // Fahrzeug faehrt 10s vorwaerts, dabei "beschleunigt" es, 
    einFahrzeug.setGeschwindigkeit(10, 10); 
   
    for (int i = 1; i <= 10; i++) {
       einFahrzeug.setGeschwindigkeit(i*i*10,i*i*10);
       einFahrzeug.fahreVorwaertsZeit(1);
      }
    
    // anschliessend bleibt es 2s stehen und dann faehrt es mit 
    einFahrzeug.setGeschwindigkeit(0,0);
    einFahrzeug.fahreRueckwaertsZeit(2);
    // kleinster Geschwindigkeit 5s rueckwaerts	
    einFahrzeug.setGeschwindigkeit(10, 10);
    einFahrzeug.fahreRueckwaertsZeit(5);


  }
  // Ende Methoden

}
