
import lejos.nxt.*;

public class Fahrzeug
extends Object
implements FahrzeugInterface, AnzeigenInterface, TouchSensorInterface, SensorConstants {
public NXTRegulatedMotor rechterMotor;
public NXTRegulatedMotor linkerMotor;
public final float DURCHMESSER_REIFEN = 5.6f;
public final float ABSTAND_ACHSE = 11.5f;

public Fahrzeug() {
rechterMotor = Motor.A;
linkerMotor = Motor.C;
}
public Fahrzeug(int geschwindigkeitLinks, int geschwindigkeitRechts) {
	rechterMotor = Motor.A;
	linkerMotor = Motor.C;
	Motor.A.setSpeed(geschwindigkeitRechts);
	Motor.A.setSpeed(geschwindigkeitLinks);
}
public float getDurchmesserReifen(){
return DURCHMESSER_REIFEN;
}
public float getAbstandAchse() {
return 	ABSTAND_ACHSE;
}
public int getGeschwindigkeitRechts() {
	int geschwindigkeitRechts = Motor.A.getSpeed();
	return geschwindigkeitRechts;
}
public int getGeschwindigkeitLinks() {
	int geschwindigkeitLinks = Motor.C.getSpeed();
	return geschwindigkeitLinks;
}
public void setGeschwindigkeit(int links, int rechts) {
	Motor.C.setSpeed(links);
	Motor.A.setSpeed(rechts);
	
}
public void fahreVorwaerts() {
	
	Motor.A.forward();
	Motor.C.forward();
}
public void fahreVorwaertsStrecke(int streckeInCm) { // 0.04799722222 sind eine Drehungseinheit, 17.279 sind 360 Drehungseinheiten, 20.8345390368 Drehungseinheiten sind 1 cm
	Motor.A.setSpeed(streckeInCm*21);
	Motor.C.setSpeed(streckeInCm*21);
	Motor.A.forward();
	Motor.C.forward();
	try {
		Thread.sleep(1000);
	} catch(InterruptedException e) {}
}
public void fahreVorwaertsZeit(int zeitInsek) {
	
	Motor.A.forward();
	Motor.C.forward();
	try {
		Thread.sleep(zeitInsek*1000);
	} catch(InterruptedException e) {}
	
}
public void fahreRueckwaerts() {
	
	Motor.A.backward();
	Motor.C.backward();
}
public void fahreRueckwaertsStrecke(int streckeInCm) {
	Motor.A.setSpeed(streckeInCm*21);    
	Motor.C.setSpeed(streckeInCm*21);
	Motor.A.backward();
	Motor.C.backward();
	try {
		Thread.sleep(1000);
	} catch(InterruptedException e) {}
}
public void fahreRueckwaertsZeit(int zeitInSek) {
	
	Motor.A.backward();
	Motor.C.backward();
	try {
		Thread.sleep(zeitInSek*1000);
	} catch(InterruptedException e) {}
}
public void dreheRechts() {
	Motor.A.stop(true);
	Motor.C.stop();
	Motor.A.backward();
}
public void dreheRechtsGrad(int winkel) {
	Motor.A.stop(true);
	Motor.C.stop();
	Motor.C.rotate((int) ((420f/85)*winkel));
}
public void dreheRechtsZeit(int zeitInSek) {
	Motor.A.stop(true);
	Motor.C.stop();
	Motor.A.backward();
	try {
		Thread.sleep(zeitInSek*1000);
	} catch(InterruptedException e) {}
}
public void dreheLinks() {
	Motor.A.stop(true);
	Motor.C.stop();
	Motor.C.backward();
}
public void dreheLinksGrad(int winkel) {
	Motor.A.stop(true);
	Motor.C.stop();
	Motor.A.rotate((420/83)*winkel);
}
public void dreheLinksZeit(int zeitInSek) {
	Motor.A.stop(true);
	Motor.C.stop();
	Motor.C.backward();
	try {
		Thread.sleep(zeitInSek*1000);
	} catch(InterruptedException e) {}
}
public void stoppe() {
	Motor.A.stop(true);
	Motor.C.stop();
}
public void zeigeText(String text) {
	LCD.drawString(text,3,4);
}
public void zeigeZahl(float zahl) {
	String s = Float.toString(zahl);
	LCD.drawString(s, 3, 4);
}
@Override
public boolean isPressed() {
	// TODO Auto-generated method stub
	return new TouchSensor(SensorPort.S1).isPressed();
	}
	

}
