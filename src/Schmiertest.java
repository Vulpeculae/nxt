import lejos.nxt.Button;
import lejos.nxt.LCD;
import lejos.nxt.Motor;

public class Schmiertest {
public static void main(String[]args) {
	Button.ENTER.waitForPressAndRelease();
	LCD.drawString("Schmiertest", 3, 4);
	LCD.refresh();
	
	Motor.A.setSpeed(360);
	Motor.C.setSpeed(360);
	Motor.A.forward();
	Motor.C.forward();
	try {
		Thread.sleep(1000);
	} catch(InterruptedException e) {}
	Motor.A.stop(true);
	Motor.C.stop();
}
}
